//Subject:     CO project 2 - Test Bench
//--------------------------------------------------------------------------------
//Version:     1
//--------------------------------------------------------------------------------
//Writer:      
//----------------------------------------------
//Date:        
//----------------------------------------------
//Description: 
//--------------------------------------------------------------------------------
`define CYCLE_TIME 10            
`define END_COUNT 250
module TestBench;

//Internal Signals
reg         CLK;
reg         RST;
integer     count;
integer     handle;
integer     end_count;
//Greate tested modle  
Simple_Single_CPU cpu(
        .clk_i(CLK),
        .rst_i(RST)
);
 
//Main function

always #(`CYCLE_TIME/2) CLK = ~CLK;    

initial  begin
    CLK = 0;
    RST = 0;
    count = 0;
    end_count=25;
    #(`CYCLE_TIME)      RST = 1;
    #(`CYCLE_TIME*`END_COUNT)    $finish;
    $dumpfile("test.vcd");
    $dumpvars(0,cpu);
end

always@(posedge CLK) begin
    count = count + 1;
    if( count == `END_COUNT )
    begin 
        $display("r0= %d, r1= %d, r2= %d, r3= %d, r4= %d, r5= %d, r6= %d, r7= %d",cpu.RF.Reg_File[0], cpu.RF.Reg_File[1], cpu.RF.Reg_File[2], cpu.RF.Reg_File[3], cpu.RF.Reg_File[4], 
                  cpu.RF.Reg_File[5], cpu.RF.Reg_File[6], cpu.RF.Reg_File[7]);
        $display("r8= %d, r=9 %d, r10=%d, r11=%d, r12=%d, r13=%d, r14=%d, r15=%d",cpu.RF.Reg_File[8], cpu.RF.Reg_File[9], cpu.RF.Reg_File[10], cpu.RF.Reg_File[11], cpu.RF.Reg_File[12], 
                  cpu.RF.Reg_File[13], cpu.RF.Reg_File[14], cpu.RF.Reg_File[15]);
        $display("r16=%d, r17=%d, r18=%d, r19=%d, r20=%d, r21=%d, r22=%d, r23=%d",cpu.RF.Reg_File[16], cpu.RF.Reg_File[17], cpu.RF.Reg_File[18], cpu.RF.Reg_File[19], cpu.RF.Reg_File[20], 
                  cpu.RF.Reg_File[21], cpu.RF.Reg_File[22], cpu.RF.Reg_File[23]);
        $display("r24=%d, r25=%d, r26=%d, r27=%d, r28=%d, r29=%d, r30=%d, r31=%d",cpu.RF.Reg_File[24], cpu.RF.Reg_File[25], cpu.RF.Reg_File[26], cpu.RF.Reg_File[27], cpu.RF.Reg_File[28], 
                  cpu.RF.Reg_File[29], cpu.RF.Reg_File[30], cpu.RF.Reg_File[31]);

        $display("m0= %d, m1= %d, m2= %d, m3= %d, m4= %d, m5= %d, m6= %d, m7= %d",cpu.Data_memory.memory[0], cpu.Data_memory.memory[1], cpu.Data_memory.memory[2], cpu.Data_memory.memory[3], cpu.Data_memory.memory[4],
                  cpu.Data_memory.memory[5], cpu.Data_memory.memory[6], cpu.Data_memory.memory[7]);
        $display("m8= %d, m=9 %d, m10=%d, m11=%d, m12=%d, m13=%d, m14=%d, m15=%d",cpu.Data_memory.memory[8], cpu.Data_memory.memory[9], cpu.Data_memory.memory[10], cpu.Data_memory.memory[11], cpu.Data_memory.memory[12],
                  cpu.Data_memory.memory[13], cpu.Data_memory.memory[14], cpu.Data_memory.memory[15]);
        $display("m16=%d, m17=%d, m18=%d, m19=%d, m20=%d, m21=%d, m22=%d, m23=%d",cpu.Data_memory.memory[16], cpu.Data_memory.memory[17], cpu.Data_memory.memory[18], cpu.Data_memory.memory[19], cpu.Data_memory.memory[20],
                  cpu.Data_memory.memory[21], cpu.Data_memory.memory[22], cpu.Data_memory.memory[23]);
        $display("m24=%d, m25=%d, m26=%d, m27=%d, m28=%d, m29=%d, m30=%d, m31=%d",cpu.Data_memory.memory[24], cpu.Data_memory.memory[25], cpu.Data_memory.memory[26], cpu.Data_memory.memory[27], cpu.Data_memory.memory[28],
                  cpu.Data_memory.memory[29], cpu.Data_memory.memory[30], cpu.Data_memory.memory[31]);
    end
    else ;
end

endmodule
