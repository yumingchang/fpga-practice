//Subject:     CO project 2 - ALU Controller
//--------------------------------------------------------------------------------
//Version:     1
//--------------------------------------------------------------------------------
//Writer:      
//----------------------------------------------
//Date:        
//----------------------------------------------
//Description: 
//--------------------------------------------------------------------------------

module ALU_Ctrl(
          funct_i,
          ALUOp_i,
          ALUCtrl_o,
		  ALUSrc1_o,
		  jr_en_o
          );
          
//I/O ports 
input      [6-1:0] funct_i;
input      [3-1:0] ALUOp_i;
output     [4-1:0] ALUCtrl_o;    
output             ALUSrc1_o;
output             jr_en_o;

//Internal Signals
reg        [4-1:0] ALUCtrl_o;
reg                ALUSrc1_o;
reg                jr_en_o;
wire       [9-1:0] binded_input;
wire       [8-1:0] shift_logical;

//Parameter
assign binded_input = { ALUOp_i, funct_i };
assign shift_logical = { binded_input[8:2], binded_input[0] };  //***

//Select exact operation
always @* begin
    casex( binded_input )
        9'b000100000: ALUCtrl_o <= 4'd0;    // ADD
		9'b000100010: ALUCtrl_o <= 4'd1;    // SUB
		9'b000100100: ALUCtrl_o <= 4'd2;    // AND
		9'b000100101: ALUCtrl_o <= 4'd3;    // OR
		9'b000101010: ALUCtrl_o <= 4'd15;   // slt
		9'b000000000: ALUCtrl_o <= 4'd4;    // shift left logic
		9'b000000010: ALUCtrl_o <= 4'd5;    // shift right logic
		9'b000000100: ALUCtrl_o <= 4'd4;    // shift left logic variable
		9'b000000110: ALUCtrl_o <= 4'd5;    // shift right logic variable
		9'b000011000: ALUCtrl_o <= 4'd7;    // MUL
		9'b000001000: ALUCtrl_o <= 4'd2;    // jr #other options : 4 5 6 7
		9'b001xxxxxx: ALUCtrl_o <= 4'd0;    // ( ALU = add ) lw, sw, addi
        9'b010xxxxxx: ALUCtrl_o <= 4'd1;    // ( ALU = sub ) beq
		9'b011xxxxxx: ALUCtrl_o <= 4'd3;    // ( ALU = OR ) ori
		9'b100xxxxxx: ALUCtrl_o <= 4'd8;    // ( ALU = src1_i ) BNEZ
		9'b101xxxxxx: ALUCtrl_o <= 4'd6;    // ( ALU = lui ) LUI
		9'b110xxxxxx: ALUCtrl_o <= 4'd15;   // ( ALU = SLT ) SLT
		9'b111xxxxxx: ALUCtrl_o <= 4'bxxxx; // jump
    endcase
	ALUSrc1_o <= ( ( binded_input == 9'b000000010 ) || ( binded_input == 9'b000000000 ) );
	jr_en_o   <= ( binded_input == 9'b000001000 )? 1 : 0;
end

endmodule     





                    
                    