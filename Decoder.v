//Subject:     CO project 2 - Decoder
//--------------------------------------------------------------------------------
//Version:     1
//--------------------------------------------------------------------------------
//Writer:      Luke
//----------------------------------------------
//Date:        2010/8/16
//----------------------------------------------
//Description: 
//--------------------------------------------------------------------------------

module Decoder(
    instr_op_i,
    RegWrite_o,
    ALU_op_o,
    ALUSrc2_o,
    RegDst_o,
    Branch_o,
    MemRead_o,
    MemWrite_o,
    MemToReg_o,
    Jump_o,
    BranchType_o
    );
     
//I/O ports
input  [6-1:0] instr_op_i;

output         RegWrite_o;
output [3-1:0] ALU_op_o;
output         ALUSrc2_o;
output [2-1:0] RegDst_o;
output         Branch_o;
output         MemRead_o;
output         MemWrite_o;
output [2-1:0] MemToReg_o;
output         Jump_o;
output [2-1:0] BranchType_o;
 
//Internal Signals
reg    [3-1:0] ALU_op_o;
reg            ALUSrc2_o;
reg            RegWrite_o;
reg    [2-1:0] RegDst_o;
reg            Branch_o;
reg            MemRead_o;
reg            MemWrite_o;
reg    [2-1:0] MemToReg_o;
reg            Jump_o;
reg    [2-1:0] BranchType_o;
//Parameter


//Main function
always @* begin
    case( instr_op_i )
        6'b000000:begin              // R type
            RegDst_o <= 2'b01;
            ALU_op_o <= 3'b000;
            ALUSrc2_o <= 1'b0;
            RegWrite_o <= 1'b1;
            Branch_o <= 1'b0;
            MemRead_o <= 1'b0;
            MemWrite_o <= 1'b0;
            MemToReg_o <= 2'b00;
            Jump_o <= 1'b0;
            BranchType_o <= 2'bxx;
        end
        6'b100011:begin              // load
            RegDst_o <= 2'b00;
            ALU_op_o <= 3'b001;
            ALUSrc2_o <= 1'b1;
            RegWrite_o <= 1'b1;
            Branch_o <= 1'b0;
            MemRead_o <= 1'b1;
            MemWrite_o <= 1'b0;
            MemToReg_o <= 2'b01;
            Jump_o <= 1'b0;
            BranchType_o <= 2'bxx;
        end
        6'b101011:begin              // store
            RegDst_o <= 2'bxx;
            ALU_op_o <= 3'b001;
            ALUSrc2_o <= 1'b1;
            RegWrite_o <= 1'b0;
            Branch_o <= 1'b0;
            MemRead_o <= 1'b0;
            MemWrite_o <= 1'b1;
            MemToReg_o <= 2'bxx;
            Jump_o <= 1'b0;
            BranchType_o <= 2'bxx;
        end
        6'b000010:begin              // jump
            RegDst_o <= 2'bxx;
            ALU_op_o <= 3'b111;
            ALUSrc2_o <= 1'bx;
            RegWrite_o <= 1'b0;
            Branch_o <= 1'b0;
            MemRead_o <= 1'b0;
            MemWrite_o <= 1'b0;
            MemToReg_o <= 2'bxx;
            Jump_o <= 1'b1;
            BranchType_o <= 2'bxx;
        end
        6'b000100:begin               // branch equal
            RegDst_o <= 2'bxx;
            ALU_op_o <= 3'b010;
            ALUSrc2_o <= 1'b0;
            RegWrite_o <= 1'b0;
            Branch_o <= 1'b1;
            MemRead_o <= 1'b0;
            MemWrite_o <= 1'b0;
            MemToReg_o <= 2'bxx;
            Jump_o <= 1'b0;
            BranchType_o <= 2'b00;
        end
        6'b000111:begin              // branch greater than
            RegDst_o <= 2'bxx;
            ALU_op_o <= 3'b010;
            ALUSrc2_o <= 1'b0;
            RegWrite_o <= 1'b0;
            Branch_o <= 1'b1;
            MemRead_o <= 1'b0;
            MemWrite_o <= 1'b0;
            MemToReg_o <= 2'bxx;
            Jump_o <= 1'b0;
            BranchType_o <= 2'b01;
        end
        6'b000101:begin              // branch not equal zero
            RegDst_o <= 2'bxx;
            ALU_op_o <= 3'b100;
            ALUSrc2_o <= 1'b0;
            RegWrite_o <= 1'b0;
            Branch_o <= 1'b1;
            MemRead_o <= 1'b0;
            MemWrite_o <= 1'b0;
            MemToReg_o <= 2'bxx;
            Jump_o <= 1'b0;
            BranchType_o <= 2'b11;
        end
        6'b000001:begin              // branch greater or equal zero
            RegDst_o <= 2'bxx;
            ALU_op_o <= 3'b100;
            ALUSrc2_o <= 1'b0;
            RegWrite_o <= 1'b0;
            Branch_o <= 1'b1;
            MemRead_o <= 1'b0;
            MemWrite_o <= 1'b0;
            MemToReg_o <= 2'bxx;
            Jump_o <= 1'b0;
            BranchType_o <= 2'b10;
        end
        6'b001111:begin              // load upper immediate
            RegDst_o <= 2'b00;
            ALU_op_o <= 3'b101;
            ALUSrc2_o <= 1'b1;
            RegWrite_o <= 1'b1;
            Branch_o <= 1'b0;
            MemRead_o <= 1'b0;
            MemWrite_o <= 1'b0;
            MemToReg_o <= 2'b00;
            Jump_o <= 1'b0;
            BranchType_o <= 2'bxx;
        end
        6'b001101:begin              // or immediate
            RegDst_o <= 2'b00;
            ALU_op_o <= 3'b011;
            ALUSrc2_o <= 1'b1;
            RegWrite_o <= 1'b1;
            Branch_o <= 1'b0;
            MemRead_o <= 1'b0;
            MemWrite_o <= 1'b0;
            MemToReg_o <= 2'b10;
            Jump_o <= 1'b0;
            BranchType_o <= 2'bxx;
        end
        6'b000011:begin              // jump and load
            RegDst_o <= 2'b10;       // input == 31
            ALU_op_o <= 3'b111;
            ALUSrc2_o <= 1'bx;
            RegWrite_o <= 1'b1;
            Branch_o <= 1'b0;
            MemRead_o <= 1'b0;
            MemWrite_o <= 1'b0;
            MemToReg_o <= 2'b11;
            Jump_o <= 1'b1;
            BranchType_o <= 2'bxx;
        end
        6'b001010:begin               // set less than immediate
            RegDst_o <= 2'b00;
            ALU_op_o <= 3'b110;
            ALUSrc2_o <= 1'b1;
            RegWrite_o <= 1'b1;
            Branch_o <= 1'b0;
            MemRead_o <= 1'b0;
            MemWrite_o <= 1'b0;
            MemToReg_o <= 2'b00;
            Jump_o <= 1'b0;
            BranchType_o <= 2'bxx;
        end
        6'b001000:begin              // addi
            RegDst_o <= 2'b00;
            ALU_op_o <= 3'b001;
            ALUSrc2_o <= 1'b1;
            RegWrite_o <= 1'b1;
            Branch_o <= 1'b0;
            MemRead_o <= 1'b0;
            MemWrite_o <= 1'b0;
            MemToReg_o <= 2'b00;
            Jump_o <= 1'b0;
            BranchType_o <= 2'bxx;
		end
		//6'b:begin              //
        //    RegDst_o <=;
        //    ALU_op_o <=;
        //    ALUSrc2_o <=;
        //    RegWrite_o <=;
        //    Branch_o <=;
        //    MemRead_o <=;
        //    MemWrite_o <=;
        //    MemToReg_o <=;
        //    Jump_o <=;
        //    BranchType_o <=;
		//end
        //default:
            
    endcase
end

endmodule





                    
                    