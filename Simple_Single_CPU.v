//Subject:     CO project 2 - Simple Single CPU
//--------------------------------------------------------------------------------
//Version:     1
//--------------------------------------------------------------------------------
//Writer:      
//----------------------------------------------
//Date:        
//----------------------------------------------
//Description: 
//--------------------------------------------------------------------------------
module Simple_Single_CPU(
        clk_i,
        rst_i
);
        
//I/O port
input         clk_i;
input         rst_i;

// Internal Signals
wire  [32-1:0] pc_instruction;
wire  [32-1:0] pc_add_4;
wire  [32-1:0] sign_extension;
wire  [32-1:0] branch_dist_pc;
wire  [32-1:0] branch_target;
wire  [32-1:0] branch_pc;
wire  [32-1:0] instruction;
wire  [32-1:0] pc_next;
wire  [32-1:0] reg_write_data;
wire  [32-1:0] alu_src_1;
wire  [32-1:0] alu_src_2;
wire  [32-1:0] RT_out;
wire  [32-1:0] ALU_result;
wire  [32-1:0] Memory_out;
wire  [32-1:0] RS_out;

wire  [28-1:0] jump_addr_bottom;

wire   [5-1:0] write_reg;

wire   [4-1:0] ALUCtrl;
wire   [4-1:0] unused_bits;

wire   [3-1:0] ALUOp;

wire   [2-1:0] BranchType;
wire   [2-1:0] RegDst;
wire   [2-1:0] Mem_to_Reg;

wire           branch_final;
wire           Jump;
wire           branch_mux_output;
wire           Branch;
wire           RegWrite;
wire           ALU_src1;
wire           ALU_src2;
wire           MemRead;
wire           MemWrite;
wire           Zero_flag;
wire           jr;

// Internal signal circuits
assign branch_final = Branch & branch_mux_output;

//Greate componentes
ProgramCounter PC(
        .clk_i( clk_i ),      
        .rst_i( rst_i ),     
        .pc_in_i( pc_next ),   
        .pc_out_o( pc_instruction ) 
);
    
Adder Adder1(
        .src1_i( pc_instruction ),     
        .src2_i( 32'd4 ),     
        .sum_o( pc_add_4 )    
);
    
Instr_Memory IM(
        .pc_addr_i( pc_instruction ),  
        .instr_o( instruction )    
);

MUX_4to1 #(.size(5)) Mux_Write_Reg(
        .data0_i( instruction[20:16] ),
        .data1_i( instruction[15:11] ),
        .data2_i( 5'd31 ),
        .data3_i( 5'd0 ),
        .select_i( RegDst ),
        .data_o( write_reg )
);    
        
Reg_File RF(
        .clk_i( clk_i ),      
        .rst_i( rst_i ),     
        .RSaddr_i( instruction[25:21] ),  
        .RTaddr_i( instruction[20:16] ),  
        .RDaddr_i( write_reg ),  
        .RDdata_i( reg_write_data ), 
        .RegWrite_i( RegWrite ),
        .RSdata_o( RS_out ),  
        .RTdata_o( RT_out )   
);
    
Decoder Decoder(
        .instr_op_i( instruction[31:26] ),
        .RegWrite_o( RegWrite ),
        .ALU_op_o( ALUOp ),
        .ALUSrc2_o( ALU_src2 ),
        .RegDst_o( RegDst ),
        .Branch_o( Branch ),
        .MemRead_o( MemRead ),
        .MemWrite_o( MemWrite ),
        .MemToReg_o( Mem_to_Reg ),
        .Jump_o( Jump ) ,
        .BranchType_o( BranchType )
);

ALU_Ctrl AC(
        .funct_i( instruction[5:0] ),   
        .ALUOp_i( ALUOp ),   
        .ALUCtrl_o( ALUCtrl ),
		.ALUSrc1_o( ALU_src1 ),
		.jr_en_o( jr )
);
    
Sign_Extend SE(
        .data_i( instruction[15:0] ),
        .data_o( sign_extension )
);

MUX_2to1 #(.size(32)) Mux_ALUSrc1(
        .data0_i( RS_out ),
        .data1_i( { 27'd0, sign_extension[10:6] } ),
        .select_i( ALU_src1 ),
        .data_o( alu_src_1 )
); 

MUX_2to1 #(.size(32)) Mux_ALUSrc2(
        .data0_i( RT_out ),
        .data1_i( sign_extension ),
        .select_i( ALU_src2 ),
        .data_o( alu_src_2 )
);    
        
ALU ALU(
        .src1_i( alu_src_1 ),
        .src2_i( alu_src_2 ),
        .ctrl_i( ALUCtrl ),
        .result_o( ALU_result ),
        .zero_o( Zero_flag )
);
        
Adder Adder2(
        .src1_i( pc_add_4 ),     
        .src2_i( branch_dist_pc ),     
        .sum_o( branch_target )      
);
        
Shift_Left_Two_32 Shifter(
        .data_i(sign_extension),
        .data_o(branch_dist_pc)
);

Shift_Left_Two_32 Jump_Addr(
        .data_i( { 6'd0, instruction[25:0] } ),
        .data_o( { unused_bits, jump_addr_bottom } )
);

Data_Memory Data_memory(
		.clk_i( clk_i ),
		.addr_i( ALU_result ),
		.data_i( RT_out ),
		.MemRead_i( MemRead ),
		.MemWrite_i( MemWrite ),
		.data_o( Memory_out )
);
        
MUX_2to1 #(.size(32)) Mux_PC_Source_branch(
        .data0_i( pc_add_4 ),
        .data1_i( branch_target ),
        .select_i( branch_final ),
        .data_o( branch_pc )
);

MUX_4to1 #(.size(32)) Mux_PC_Source_jump (
        .data0_i( branch_pc ),
        .data1_i( {pc_add_4[31:28], jump_addr_bottom } ),
		.data2_i( RS_out ),
        .data3_i( RS_out ),
        .select_i( { jr , Jump } ),
        .data_o( pc_next )
);    

MUX_4to1 #(.size(1)) Branch_Selection(
        .data0_i( Zero_flag ),
        .data1_i( ~( ALU_result[31] | Zero_flag ) ),
		.data2_i( ~ALU_result[31] ),
        .data3_i( ~Zero_flag ),
        .select_i( BranchType ),
        .data_o( branch_mux_output )
);

MUX_4to1 #(.size(32)) MemToReg_select(
        .data0_i( ALU_result ),
        .data1_i( Memory_out ),
		.data2_i( sign_extension ),
        .data3_i( pc_add_4 ),
        .select_i( Mem_to_Reg ),
        .data_o( reg_write_data )
);
endmodule
          


